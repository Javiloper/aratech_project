<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\WorkoutsController;
use App\Http\Controllers\ExercisesWorkoutController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard.index');
})->middleware(['auth'])->name('dashboard.index');

Route::get('dashboard', function () {
    return view('dashboard.index');
});



// WORKOUT ROUTES

Route::get('/workout/create', [WorkoutsController::class, 'create'])
->name('workout.create');


Route::get('/workout/list', [WorkoutsController::class, 'list'])
->name('workout.list');

Route::get('/workout/get-rpes', [WorkoutsController::class, 'getRpes'])
->name('workout.rpes');

Route::post('/workout/save', [WorkoutsController::class, 'save'])
->name('workout.save');

Route::post('/workout/delete', [WorkoutsController::class, 'delete'])
->name('workout.delete');

// END WORKOUT ROUTES




// EXERCISES WORKOUT ROUTES

Route::get('/exercises-workout/create/{id_workout}', [ExercisesWorkoutController::class, 'create'])
->name('exercisesworkout.create');


Route::post('/exercises-workout/save', [ExercisesWorkoutController::class, 'save'])
->name('exercisesworkout.save');

// END EXERCISES WORKOUT ROUTES



require __DIR__.'/auth.php';
