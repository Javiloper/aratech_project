<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\Workouts;

class WorkoutsController extends Controller
{
    //

    public function create() {
        return view('workouts.create');
    }

    public function list() {

        $workouts = Workouts::where('user_id', Auth::id())->get();

        return view('workouts.list')->with('workouts', $workouts);  
    }

    public function getRpes() {

        $workouts = Workouts::where('user_id', Auth::id())->get();
        return response()->json([
            'data' => $workouts
        ]);
    }

    public function save(Request $request) {
        
        $workout = new Workouts;
        $workout->title         = $request['title'];
        $workout->description   = $request['description'];
        $workout->rpe           = $request['rpe'];
        $workout->user_id       = Auth::id();

        if($workout->save()) {
            return redirect('/workout/list');
        }
    }

    public function delete(Request $request) {

        $deleted    = DB::table('workouts')->where('id', '=', $request['id'])->delete();
        $deleted2   = DB::table('exercises_workouts')->where('workouts_id', '=', $request['id'])->delete();

        return response()->json([
            'data' => $deleted
        ]);
    }
}
