<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ExercisesWorkout;

class ExercisesWorkoutController extends Controller
{
    
    public function create($id_workout) {
        return view('exercises-workout.create')->with('id_workout', $id_workout);
    }

    public function save(Request $request) {

        foreach ($request['exercise'] as $key => $value) {
            $exerciseWorkout = new ExercisesWorkout;
            $exerciseWorkout->exercise = $value; 
            $exerciseWorkout->workouts_id = $request['id_workout']; 
            $exerciseWorkout->save();
        }

        return redirect('/workout/list');
    }
}
