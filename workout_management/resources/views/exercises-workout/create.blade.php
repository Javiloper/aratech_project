@extends('layouts.layout')

@section('content')
        
<div class="card card-dark">
    <div class="card-header">
        <h1 class='text-center'>Create a new workout</h1>
    </div>
    <form method="POST" action='/exercises-workout/save'>
        <input type='hidden' name='id_workout' value='{{$id_workout}}'>
        @csrf
        <div class="card-body" id='id_exercise_form'>

            <div class="form-group">
                <label for="id_exercise_1">Exercise</label>
                <input type="text" name='exercise[]' class="form-control" id="id_exercise_1" placeholder="Leg A, Chest-Back...">
            </div>

        </div>
        
        <div class="card-footer">
            <button type="button" class="btn btn-primary" onclick='addExercise()'>Add Exercise</button>
            <button type="submit" class="btn btn-dark">Submit</button>
        </div>
    </form>
    </div>
@endsection


<script>

    var global_count = 1;

    function addExercise() {

        global_count++;
        var new_id = 'id_exercise_' + global_count

        // Container
        var place       = document.getElementById('id_exercise_form');

        // Create elements
        var form_group  = document.createElement('div');
        var label       = document.createElement('label');
        var input       = document.createElement('input');

        // Configure elements
        form_group.classList.add('form-group');
        label.setAttribute('for', new_id);
        label.textContent = 'Exercise ' + global_count
        input.id = new_id
        input.type = 'text';
        input.name = 'exercise[]';
        input.classList.add('form-control')
        input.placeholder = 'Leg A, Chest-Back...'

        // Append elements

        place.appendChild(form_group)
        form_group.appendChild(label)
        form_group.appendChild(input)

        }

</script>