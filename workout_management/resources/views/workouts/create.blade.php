@extends('layouts.layout')

@section('content')
        

<div class="card card-dark">
    <div class="card-header">
        <h1 class='text-center'>Create a new workout</h1>
    </div>
    <form method="POST" action='/workout/save'>
        @csrf
        <div class="card-body">
            <div class="form-group">
                <label for="id_title">Title</label>
                <input type="text" name='title' class="form-control" id="id_title" placeholder="Leg A, Chest-Back...">
            </div>
            <div class="form-group">
                <label for="id_description">Description</label>
                <input type="text" name='description' class="form-control" id="id_description" placeholder="Describe your training (optional)">
            </div>
            <div class="form-group">
                <label for="id_rpe">RPE (Rate of Perceived Exertion)</label>
                <input type="number" name='rpe' class="form-control" id="id_rpe" placeholder="Effort?">
            </div>
            
        </div>
        
        <div class="card-footer">
            <button type="submit" class="btn btn-dark">Submit</button>
        </div>
    </form>
    </div>
@endsection