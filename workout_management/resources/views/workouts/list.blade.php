@extends('layouts.layout')

@section('content')


@foreach ($workouts as $workout)
    {{$workout->rpe}}   
@endforeach


<div class="card card-dark">
    <div class="card-header">
        <h1 class='text-center'>List of workouts</h1>
    </div>
    
    <div class='card-body'>
        <table class="table table-bordered" id="dtBasicExample">
            <thead>
                <tr>
                    <th style="width: 10px">#</th>
                    <th>Title</th>
                    <th>Description</th>
                    {{-- <th>Exercises</th> --}}
                    <th style="width: 40px">RPE</th>
                    <th style="width: 40px">Delete</th>
                    <th style="width: 40px">Add</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($workouts as $workout)
                <tr id='id_tr_workout_{{$workout->id}}'>
                    <td>{{$workout->id}}</td>    
                    <td>{{$workout->title}}</td>    
                    <td>{{$workout->description}}</td>    
                    {{-- <td>{{$workout->exs}}</td>     --}}
                    <td>
                        @if($workout->RPE < 5)
                            <span class="badge bg-success padding-6">{{$workout->RPE}}</span>
                        @elseif($workout->RPE >=5 && $workout->RPE < 7)
                            <span class="badge bg-warning padding-6">{{$workout->RPE}}</span>
                        @elseif($workout->RPE == 10 )
                            <span class="badge bg-danger padding-6" style='background-color: black !important;'>{{$workout->RPE}}</span>
                        @else
                            <span class="badge bg-danger padding-6" >{{$workout->RPE}}</span>
                        @endif
                    </td>    
                    <td>
                        <button onclick='deleteWorkout({{$workout->id}})' type="button" class="btn btn-danger"><i class="fa-solid fa-trash-can"></i></button>
                        
                    </td>
                    <td>
                        <button onclick='addExercises({{$workout->id}})' type="button" class="btn btn-primary"><i class="fa-solid fa-plus"></i></button>
                        
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>

        <div style='height: 2px; background: black;' class='mt-3 mb-3'></div>

        <div class='offset-md-3 col-md-6'>
            <h3 class='center-text'>RPE Record Chart</h3>
            <canvas id="chart_rpe"></canvas>
        </div>
    </div>

</div>
<script>


    function deleteWorkout(id) {

        Swal.fire({
            title: 'Delete',
            text: 'Do you want to remove this workout?',
            icon: 'warning',
            confirmButtonText: 'Yes, remove it',
            showCancelButton: true,

            }).then((result) => {
                if(result.isConfirmed) {
                        
                    var formData = new FormData();
                    formData.append('id', id)
                    var ajax_del = new XMLHttpRequest();

                    ajax_del.onreadystatechange = function() {
                        if (ajax_del.readyState == 4 ) {
                            
                            setTimeout(() => {
                                document.getElementById("id_tr_workout_"+id).remove()
                            }, 250);

                            console.log( JSON.parse(ajax_del.responseText) )
                            
                            
                        }
                    }
                    
                    ajax_del.open( "POST", "/workout/delete", true );
                    ajax_del.setRequestHeader("X-CSRF-TOKEN", document.head.querySelector("[name=csrf-token]").content );
                    ajax_del.send(formData);
                }
            })

    }

    function addExercises(id) {
        window.location.href = "/exercises-workout/create/" + id;
    }

    $(document).ready(function () {
        $('#dtBasicExample').DataTable();
        $('.dataTables_length').addClass('bs-select');



    });

    var rpes = []
    var ids = []

    var ajax = new XMLHttpRequest();
    ajax.onreadystatechange = function() {
        if (ajax.readyState == 4 ) {

            console.log( JSON.parse(ajax.responseText) )

            var data =  JSON.parse(ajax.responseText)
            data.data.forEach(element => {
                rpes.push(element['RPE'])
                ids.push(element['id'])
            });
            console.log(rpes)
        }
    }
    ajax.open( "GET", "/workout/get-rpes", true );
    ajax.send();


    setTimeout(() => {

        const labels = ids;

        const data = {
        labels: labels,
        datasets: [{
            label: 'My First dataset',
            backgroundColor: 'black',
            borderColor: 'gray',
            data: rpes
        }]
        };

        const config = {
        type: 'line',
        data: data,
        options: {
           
        }
        };

        const myChart = new Chart(
        document.getElementById('chart_rpe'),
        config
        );
    }, 1000);

</script>
@endsection